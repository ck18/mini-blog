﻿module app.posts {
    'use strict';

    export interface Post {
        Id: number
        Title: string
        Content: string
        Status: string
        Slug: string
        CreatedAt: Date
        UpdatedAt: Date
    }

    export class PostService {

        baseUrl = '/api/PostApi/';

        static $inject = ['$http'];

        constructor(private $http: ng.IHttpService) {

        }

        getAll(): ng.IPromise<Post[]> {
            return this.$http.get(this.baseUrl)
                .then((res: ng.IHttpPromiseCallbackArg<Post[]>) => {
                    return res.data;
                })
        }

        getPost(id: number): ng.IPromise<Post> {
            return this.$http.get(this.baseUrl + id)
                .then((res: ng.IHttpPromiseCallbackArg<Post>) => {
                    return res.data;
                });
        }

        update(post: Post): ng.IPromise<Post | string> {
            return this.$http.patch(this.baseUrl + post.Id, post)
                .then((res: ng.IHttpPromiseCallbackArg<Post | string>) => {
                    return res.data;
                });
        }

        deletePost(postId: number): ng.IPromise<string> {
            return this.$http.delete(this.baseUrl + postId)
                .then((res: ng.IHttpPromiseCallbackArg<string>) => {
                    return res.data;
                })
        }

        create(post: Post): ng.IPromise<any> {
            return this.$http.post(this.baseUrl + 'Create', post)
                .then((res) => {
                    return res.data;
                })
                .catch((err) => {
                    console.log(err)
                })
        }


    }

    angular
        .module('app.posts')
        .service('PostService', PostService);

}