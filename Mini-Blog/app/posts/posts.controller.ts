﻿module app.posts {
    'use strict';

    interface IPostScope {
        posts: Post[]
    }

    class PostController implements IPostScope {
        /**
         * Show Post
         */
        posts: Post[];
        /**
         * Add Post
         */
        newPost: Post;
        /**
         * Details Post
         */
        detailPost: Post;
        /**
         * Edit Post
         */
        editPost: Post;


        $onInit = () => {
            const href = window.location.href;
            const lastSegment = href.split('/');
            const id = lastSegment[lastSegment.length - 1] ? parseInt(lastSegment[lastSegment.length - 1]) : '';
            const action = lastSegment[lastSegment.length - 2];

            if (action == "Details" && typeof id == 'number') {
                this.postService.getPost(id)
                    .then((post) => {
                        this.detailPost = post;
                    });
            }

            if (action == "Edit" && typeof id == 'number') {
                this.postService.getPost(id)
                    .then((post) => {
                        this.editPost = post;
                        (() => $("#summernote").summernote('editor.insertText', post.Content))();
                    });
            }
        };

        static $inject = ['PostService', '$scope', '$window', '$sce'];

        constructor(private postService: app.posts.PostService) {
            var vm = this;
            this.postService.getAll()
                .then((posts: Post[]) => {
                    vm.posts = posts;
                })
        }

        goToCreate() {
            window.location.href = '/PostAngular/Create'
        }

        createForm() {
            this.newPost.Content = $('#summernote').code();
            this.postService.create(this.newPost)
                .then(() => {
                    alert('Created');
                    window.location.href = '/PostAngular/';
                })
        }

        editForm() {
            this.editPost.Content = $('#summernote').code();
            this.postService.update(this.editPost)
                .then(() => {
                    alert('Updated');
                    window.location.href = '/PostAngular/';
                })
        }

        deletePost(id: number) {
            this.postService.deletePost(id)
                .then(() => {
                    window.location.reload();
                });
        }

    }

    angular.module('app.posts').controller('PostController', PostController);
}