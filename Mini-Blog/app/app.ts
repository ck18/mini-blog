﻿((): void => {
    'use strict';

    var app = angular.module("app", ["app.posts"]);

    app.filter('htmlToPlaintext', function () {
        return function (text) {
            return text ? String(text).replace(/<[^>]+>/gm, '') : '';
        };
    });

    app.filter('renderHTML', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        }
    });
})();