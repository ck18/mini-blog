﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Mini_Blog.Services;

namespace Mini_Blog
{
    public class ContainerConfig
    {
        public static void RegisterContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<UserData>()
                .As<IUserData>()
                .InstancePerRequest();

            builder.RegisterType<PostData>()
                .As<IPostData>()
                .InstancePerRequest();

            builder.RegisterType<MiniBlogDbContext>()
                .InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}