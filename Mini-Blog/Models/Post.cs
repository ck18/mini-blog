﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mini_Blog.Models
{
    public class Post
    {
        [Required] [Key] public int Id { get; set; }
        [Required] public string Title { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }

        [Required] public string Status { get; set; }
        [Required] public string Slug { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime UpdatedAt { get; set; }

        [DataType(DataType.DateTime)] public DateTime? DeletedAt { get; set; }
    }
}