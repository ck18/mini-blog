﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mini_Blog.Models
{
    public class User
    {
        [Required] [Key] public int Id { get; set; }

        [Required] [DisplayName("User Name")] public string UserName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        [Required] public bool IsAdmin { get; set; } = false;
    }
}