﻿using System.Data.Entity;
using Mini_Blog.Models;

namespace Mini_Blog.Services
{
    public class MiniBlogDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}