﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Mini_Blog.Models;

namespace Mini_Blog.Services
{
    public class PostData : IPostData
    {
        private readonly MiniBlogDbContext db;

        public PostData(MiniBlogDbContext dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<Post> GetAll()
        {
            return from u in db.Posts where u.DeletedAt == null orderby u.Id select u;
        }

        public Post Get(int id)
        {
            return db.Posts.Where(post => post.DeletedAt == null).FirstOrDefault(post => post.Id == id);
        }

        public Post Get(string slug)
        {
            return db.Posts.Where(post => post.DeletedAt == null).FirstOrDefault(post => post.Slug == slug);
        }

        public IEnumerable<Post> GetAllNoTrack()
        {
            return db.Posts.AsNoTracking().Where(p => p.DeletedAt == null);
        }

        public void Add(Post post)
        {
            post.CreatedAt = DateTime.Now;
            post.UpdatedAt = DateTime.Now;
            post.DeletedAt = null;
            post.Slug = post.Slug.Replace(' ', '-').ToLower();
            db.Posts.Add(post);
            db.SaveChanges();
        }

        public void Update(Post post)
        {
            post.CreatedAt = db.Posts.AsNoTracking().First(p => p.Id == post.Id).CreatedAt;
            post.UpdatedAt = DateTime.Now;
            post.DeletedAt = null;
            var entry = db.Entry(post);
            entry.State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var post = db.Posts.Find(id);
            post.DeletedAt = DateTime.Now;
            db.SaveChanges();
        }
    }
}