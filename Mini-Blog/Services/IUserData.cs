﻿using System.Collections.Generic;
using Mini_Blog.Models;

namespace Mini_Blog.Services
{
    public interface IUserData
    {
        IEnumerable<User> GetAll();

        User Get(int id);

        User Get(string email);

        string Add(User user);

        void Update(User user);

        void Delete(int id);

        bool AttemptLogin(string email, string password);
    }
}