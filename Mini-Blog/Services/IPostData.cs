﻿using System.Collections.Generic;
using Mini_Blog.Models;

namespace Mini_Blog.Services
{
    public interface IPostData
    {
        IEnumerable<Post> GetAll();
        
        Post Get(int id);
        
        Post Get(string slug);

        void Add(Post post);

        void Update(Post post);

        void Delete(int id);
    }
}