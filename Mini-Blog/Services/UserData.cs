﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Mini_Blog.Models;

namespace Mini_Blog.Services
{
    public class UserData : IUserData
    {
        private readonly MiniBlogDbContext db;

        public UserData(MiniBlogDbContext dbContext)
        {
            db = dbContext;
        }

        public IEnumerable<User> GetAll()
        {
            return from u in db.Users orderby u.Id select u;
        }

        public User Get(int id)
        {
            return db.Users.FirstOrDefault(u => u.Id == id);
        }

        public User Get(string email)
        {
            return db.Users.FirstOrDefault(u => u.Email == email);
        }

        public string Add(User user)
        {
            if (db.Users.FirstOrDefault(u => u.Email == user.Email) != null)
            {
                return "Duplicated Email";
            }

            //First User will get admin 
            if (db.Users.Count() == 0)
            {
                user.IsAdmin = true;
            }

            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            db.Users.Add(user);
            db.SaveChanges();
            return "OK";
        }

        public void Update(User user)
        {
            var entry = db.Entry(user);
            entry.State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
        }

        public bool AttemptLogin(string email, string password)
        {
            //https://jasonwatmore.com/post/2020/07/16/aspnet-core-3-hash-and-verify-passwords-with-bcrypt
            var user1 = db.Users.FirstOrDefault(u => u.Email == email);
            return user1 != null && BCrypt.Net.BCrypt.Verify(password, user1.Password);
        }
    }
}