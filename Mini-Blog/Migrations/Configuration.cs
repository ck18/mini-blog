﻿using System.Data.Entity.Migrations;

namespace Mini_Blog.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Mini_Blog.Services.MiniBlogDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }
    } 
}