﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Mini_Blog.Models;
using Mini_Blog.Services;

namespace Mini_Blog.Controllers.Api
{
    public class PostApiController : ApiController
    {
        private IPostData db;

        public PostApiController()
        {
            db = new PostData(new MiniBlogDbContext());
        }

        [HttpGet]
        public JsonResult<IEnumerable<Post>> Get()
        {
            var model = db.GetAll();
            return Json(model);
        }

        [HttpGet]
        public object Get(int id)
        {
            var post = db.Get(id);
            if (post == null)
            {
                return NotFound();
            }

            return Json(post);
        }

        [HttpPost]
        public object Create(Post post)
        {
            if (!ModelState.IsValid)
            {
                var validation = ModelState
                    .Select(x => x.Value.Errors)
                    .Where(y => y.Count > 0)
                    .ToList();
                return Json(validation);
            }

            db.Add(post);
            return Json(post);
        }

        [HttpPatch]
        public object Edit(Post post)
        {
            if (!ModelState.IsValid)
            {
                var validation = ModelState
                    .Select(x => x.Value.Errors)
                    .Where(y => y.Count > 0)
                    .ToList();
                return Json(validation);
            }

            db.Update(post);
            return Json(post);
        }

        [HttpDelete]
        public JsonResult<string> Delete(int id)
        {
            var post = db.Get(id);
            if (post == null)
            {
                return Json("Post not Found");
            }

            db.Delete(id);
            return Json("Post deleted");
        }
    }
}