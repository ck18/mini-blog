﻿using System.Web.Mvc;
using Mini_Blog.Models;
using Mini_Blog.Services;

namespace Mini_Blog.Controllers
{
    public class PostController : Controller
    {
        private IPostData db;

        public PostController(IPostData db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (Session["LoggedIn"] == null || (string)Session["IsAdmin"] != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            var posts = db.GetAll();
            return View(posts);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            db.Add(post);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Get(id);
            if (model == null)
            {
                ViewData["Message"] = "Post do not exits";
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post, FormCollection formCollection)
        {
            if (!ModelState.IsValid)
            {
                return View(post);
            }

            db.Update(post);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var model = db.Get(id);
            return View(model);
        }

        [HttpGet]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var model = db.Get(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection formCollection)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }
    }
}