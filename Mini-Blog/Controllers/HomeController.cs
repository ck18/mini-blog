﻿using System.Web.Mvc;

namespace Mini_Blog.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Blog");
       }
    }
}