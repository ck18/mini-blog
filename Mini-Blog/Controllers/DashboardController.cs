﻿using System.Web.Mvc;
using Mini_Blog.Services;

namespace Mini_Blog.Controllers
{
    public class DashboardController : Controller
    {
        private IUserData db;

        public DashboardController(IUserData db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (Session["UserID"] == null || Session["IsAdmin"] != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            var model = db.Get((int)Session["UserID"]);

            return View(model);
        }
    }
}