﻿using System.Web.Mvc;
using Mini_Blog.Services;

namespace Mini_Blog.Controllers
{
    public class BlogController : Controller
    {
        private IPostData db;

        public BlogController(IPostData db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = db.GetAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Details(string slug)
        {
            var model = db.Get(slug);
            return View(model);
        }
    }
}