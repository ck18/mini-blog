﻿using System.Web.Mvc;

namespace Mini_Blog.Controllers
{
    public class PostAngularController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Details()
        {
            return View();
        }
    }
}