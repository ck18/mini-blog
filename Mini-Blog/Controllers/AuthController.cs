﻿using System.Configuration;
using System.Web.Mvc;
using Mini_Blog.Models;
using Mini_Blog.Services;

namespace Mini_Blog.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserData db;

        public AuthController(IUserData db)
        {
            this.db = db;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection formCollection)
        {
            var isAttemptSuccess = db.AttemptLogin(formCollection["email"], formCollection["password"]);
            if (!isAttemptSuccess)
            {
                ViewData["Message"] = "Your Credential Doesnt match with our record";
                return View();
            }

            var user = db.Get(formCollection["email"]);
            Session["UserID"] = user.Id;
            if (user.IsAdmin)
            {
                Session["IsAdmin"] = true;
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(User user, FormCollection formCollection)
        {
            if (formCollection["confirmpassword"] != user.Password)
            {
                ViewData["confirmPasswordErrorMessage"] = "Your Password do not match";
                return View();
            }

            if (!ModelState.IsValid)
            {
                return View();
            }

            var registerStatus = db.Add(user);
            if (registerStatus != "OK")
            {
                ViewData["registerStatus"] = registerStatus;
                return View();
            }

            var newUser = db.Get(user.Id);
            Session["UserID"] = newUser.Id;
            if (newUser.IsAdmin)
            {
                Session["IsAdmin"] = "true";
            }

            return RedirectToAction("Index", "Home");
        }
    }
}